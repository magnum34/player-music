﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Storage;
using Windows.Media;
using Windows.Storage.Streams;
using Windows.Storage.Pickers;


// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace PlayerMusicApp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    /// 

    public sealed partial class MainPage : Page
    {
        private List<string> paths = new List<string>();
        private IReadOnlyList<StorageFile> files;
        private static int id = 0;
        private MediaElement media;
        private StorageFolder list;
        private TagLib.File data;
        private List<IRandomAccessStreamWithContentType> stream = new List<IRandomAccessStreamWithContentType>();
        private TimeSpan TotalTime;
        private DispatcherTimer timerTime;
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if(e.Parameter != "")
            {
                NavigationContext nav = (NavigationContext)e.Parameter;
                id = nav.Activity;
                stream = nav.Stream;
                paths = nav.Paths;
                if (stream.Count > 0)
                {
                    MediaPlayer.SetSource(stream[id], stream[id].ContentType);
                    MediaPlayer.AutoPlay = false;
                    LoadDataMusic();

                }
            }
           
        }
        public MainPage()
        {
            media = new MediaElement();
            this.InitializeComponent();
            sliderTime.Value = 0;
            timerTime = new DispatcherTimer();


        }
        private void LoadDataMusic()
        {
            SimpleFile simpleFile = new SimpleFile(paths[id], stream[id].AsStream());
            TagLib.File.IFileAbstraction tag = new SimpleFileAbstraction(simpleFile);
            data = TagLib.File.Create(tag);
            TitleMusic.Text = data.Tag.Title + " - " + data.Tag.FirstArtist;

        }
        private async void appBarButtonFolder_Click(object sender, RoutedEventArgs e)
        {
            stream.Clear();
            paths.Clear();
            FolderPicker picker = new FolderPicker() { SuggestedStartLocation = PickerLocationId.MusicLibrary };
            picker.FileTypeFilter.Add(".mp3");
            list = await picker.PickSingleFolderAsync();
            if(list != null)
            {
                files = await list.GetFilesAsync(Windows.Storage.Search.CommonFileQuery.OrderByName);
            }
            if (files != null)
            {
                foreach (StorageFile file in files)
                {

                    var item = await file.OpenReadAsync();
                    paths.Add(file.Path);
                    stream.Add(item);
                    
                }
            }
            if(stream.Count > 0)
            {
                MediaPlayer.SetSource(stream[id], stream[id].ContentType);
                MediaPlayer.Pause();
                MediaPlayer.AutoPlay = false;
                LoadDataMusic();
                
            }
            

        }

        

        private void appBarButtonPlay_Click(object sender, RoutedEventArgs e)
        {
            
            MediaPlayer.Position = TimeSpan.Zero;
            sliderTime.Value = 0;
            MediaPlayer.Volume = 100;
            MediaPlayer.Play();
            timerTime.Interval = TimeSpan.FromSeconds(1.0);
            timerTime.Tick += new EventHandler<object>(time_Tick);
            timerTime.Start();

            TotalTime = MediaPlayer.NaturalDuration.TimeSpan;


        }

        private void time_Tick(object sender, object e)
        {
            if (MediaPlayer.NaturalDuration.TimeSpan.TotalSeconds > 0)
            {
                if (TotalTime.TotalSeconds > 0)
                {
                    sliderTime.Value = MediaPlayer.Position.TotalSeconds;
                }
            }
        }

        private void appBarButtonStop_Click(object sender, RoutedEventArgs e)
        {
            sliderTime.Value = 0;
            MediaPlayer.Stop();
        }

        private void appBarButtonPrevious_Click(object sender, RoutedEventArgs e)
        {
            if(id > 0)
            {
                id--;
            }
            if(stream.Count > 0)
            {
                MediaPlayer.Position = TimeSpan.Zero;
                MediaPlayer.SetSource(stream[id], stream[id].ContentType);
                LoadDataMusic();
                MediaPlayer.AutoPlay = true;
            }
            
        }

        private void appBarButtonNext_Click(object sender, RoutedEventArgs e)
        {
            if(stream.Count  > id)
            {
                id++;
            }
            if (stream.Count > 0)
            {
                MediaPlayer.Position = TimeSpan.Zero;
                MediaPlayer.SetSource(stream[id], stream[id].ContentType);
                LoadDataMusic();
                MediaPlayer.AutoPlay = true;
            }
           
            
        }

        private void SeekToMediaPosition(object sender, RangeBaseValueChangedEventArgs e)
        {
            if (TotalTime.TotalSeconds > 0)
            {
                MediaPlayer.Position = TimeSpan.FromSeconds(sliderTime.Value);
            }

        }

        private void appBarButtonPause_Click(object sender, RoutedEventArgs e)
        {
            MediaPlayer.Pause();
        }

        private void appBatButtonAllApps_Click(object sender, RoutedEventArgs e)
        {
            NavigationContext context = new NavigationContext() { Activity = id, Stream = stream, Paths = paths };
            Frame.Navigate(typeof(ListPage),context);
        }

        private void sliderVolume_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            if(MediaPlayer != null)
            {
                MediaPlayer.Volume = (double) sliderVolume.Value/100;
            }
            
        }
    }
}
