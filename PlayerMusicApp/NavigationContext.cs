﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage.Streams;

namespace PlayerMusicApp
{
    class NavigationContext
    {
        public List<IRandomAccessStreamWithContentType> Stream { set; get; }
        public int Activity { set; get; }
        public List<string> Paths { set; get; }
    }
}
