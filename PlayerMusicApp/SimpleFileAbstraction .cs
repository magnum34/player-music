﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlayerMusicApp
{
    class SimpleFileAbstraction : TagLib.File.IFileAbstraction
    {
        private SimpleFile file;
        public SimpleFileAbstraction(SimpleFile file)
        {
            this.file = file;
        }
        public string Name
        {
            get
            {
                return file.Name;
            }
        }

        public Stream ReadStream
        {
            get
            {
                return file.Stream;
            }
        }

        public Stream WriteStream
        {
            get
            {
                return file.Stream;
            }
        }

        public void CloseStream(Stream stream)
        {
            stream.Position = 0;
        }
    }
}
