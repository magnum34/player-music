﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace PlayerMusicApp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ListPage : Page
    {

        NavigationContext nav;
        private TagLib.File data;
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            nav =(NavigationContext) e.Parameter;
            Load();
        }
        public ListPage()
        {
            this.InitializeComponent();


        }

        private void Load()
        {
            
            var items = ListMusic.Items;
            ListMusic.Items.Clear();
            for (int i = 0; i< nav.Stream.Count-1; i++)
            {
                SimpleFile simpleFile = new SimpleFile(nav.Paths[i], nav.Stream[i].AsStream());
                TagLib.File.IFileAbstraction tag = new SimpleFileAbstraction(simpleFile);
                data = TagLib.File.Create(tag);
                string name = (i+1)+". " + data.Tag.Title+" - " + data.Tag.FirstArtist + " "+data.Tag.Year;
               
                items.Add(name);
                
                
            }
            data = null;



        }
        private void Back_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(MainPage), nav);
        }

        private void ListMusic_ItemClick(object sender, ItemClickEventArgs e)
        {

            var item = e.ClickedItem;
            var items = ListMusic.Items;
            int id = items.IndexOf(item);
            nav.Activity = id;
            this.Frame.Navigate(typeof(MainPage), nav);


        }

        private void ListMusic_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
        }
    }
}
